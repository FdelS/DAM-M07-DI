NF 1.1: Confecció d'interfícies d'usuari
===
- [Presentació del mòdul](./DAM1_M07_DI_presentacio_2016-2017.pdf)
- [Introducció als llenguatges de programació](./DAM1_M07_UF1.NF1_Introduccio_lenguatges_prog.pdf)
- [Introducció a JavaFX](./DAM1_M07_UF1.NF1_Te-01_Introduccio_JavaFX.pdf)
- Creació d'una interfície gràfica bàsica
	- [BasicaFXML](./src/BasicaFXML)
	- [BasicaFXML_CSS](./src/BasicaFXML_CSS)
- Contenidors
- Components comuns de formulari: buttons, labels, textbox, ...
- Menús i barra d'eines
