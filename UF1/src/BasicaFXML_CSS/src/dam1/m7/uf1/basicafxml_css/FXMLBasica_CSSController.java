package dam1.m7.uf1.basicafxml_css;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class FXMLBasica_CSSController implements Initializable {
    
    private final int MIN_ANY = 1970;
    private final int MAX_ANY = 2016;
    
    @FXML private TextField tfNom;
    @FXML private TextField tfCognoms;
    @FXML private ChoiceBox cbAny;
    @FXML private TextField tfResultat;
       
    
    @FXML
    private void bMostrarAction(ActionEvent event) {
        tfResultat.clear();
        if (tfNom.getText().isEmpty()) {
            mostraAlerta("No has introduït el nom");
        } else {
            if (tfCognoms.getText().isEmpty()) {
                mostraAlerta("No has introduït els cognoms");
            } else {
                if (cbAny.getSelectionModel().isEmpty()) {
                    mostraAlerta("Selecciona una data");
                }
                else {
                    String msg = tfCognoms.getText() + ", " + tfNom.getText() + " Any: " + cbAny.getValue().toString();
                    System.out.println(msg);
                    tfResultat.setText(msg);                    
                }                    
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        List anys = new ArrayList();
        tfNom.clear();
        tfCognoms.clear();
        tfResultat.clear();
        
        for (int any = MIN_ANY; any < MAX_ANY; any++) {
            anys.add(Integer.toString(any));
        }
        
        cbAny.setItems(FXCollections.observableArrayList(anys));
    }
    
    private void mostraAlerta (String missatge) {
        Alert alert = new Alert(AlertType.ERROR, missatge, ButtonType.YES);
        alert.showAndWait();
//
//        if (alert.getResult() == ButtonType.YES) {
//            // fes alguna cosa ;-)
//        }
    }
}