Materials del mòdul [M07 - Desenvolupament d'interfícies](http://www.ies-eugeni.cat/course/view.php?id=781) del *Cicle Formatiu de Grau Superior de Desenvolupament d'Aplicacions Multiplataforma* al l'Institut públic [Eugeni d'Ors](http://www.ies-eugeni.cat) de Vilafranca del Penedès
===

Índex
-----

* UF 1: Disseny i implementació d’interfícies
	1. [Confecció d'interfícies d'usuari](UF1/nf1.1-Confeccio_interficies_usuari.md)
	2. Usabilitat
	3. Creació de components visuals
	4. Generació d'interfícies a partir de documents XML
	5. Confecció d'informes
* UF 2: Preparació i distribució d’aplicacions
	1. Realització de proves
	2. Documentació d’aplicacions
	3. Distribució d’aplicacions